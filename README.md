## EJERCICIO JAVA - RETRATOS ROBOT ##
---


   <span style="color:blue">**C.F.G.S. Desarrollo de Aplicaciones Web**</span>   
   <span style="color:blue">**Módulo de Programación - Curso 2020/2021** </span>  
   <span style="color:blue">**Autor: Germán Villar García**</span>



Programa para crear retratos robot con cadenas de caracteres.  
Una vez ejecutado el programa, el usuario puede crear tantos retratos como desee antes de finalizar.  
Los retratos se crean eligiendo los rasgos de cada facción con las opciones siguientes:

~~~
PELO	              	OJOS                   NARIZ
1. wwwwwwwww	      	1. | O   O |           1. @   J   @
2. \\\//////		2. |-(· ·)-|           2. (   "   )
3. |"""""""|		3. |-(o o)-|           3. [   j   ]
4. |||||||||		4. | \   / |           4. <   -   >

BOCA                  	BARBILLA
1. |  ===  |          	1. |_______|
2. |   -   |          	2. |,,,,,,,|
3. |  ___  |                
4. |  ---  |          
~~~
Una vez elegidos los rasgos de cada facción, se mostrará el retrato por pantalla. Por ejemplo las opciones 3, 2, 1, 4 y 2 para pelo, ojos, nariz, boca y barbilla respectivamente, muestran el siguiente retrato:
~~~
|"""""""|
|-(· ·)-|
@   J   @
|  ---  |
\,,,,,,,/
~~~